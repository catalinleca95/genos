# Genos App

## Getting started

- **Go into 'backend' folder and run**

```
npm install
npm run start
```

After the server has started open a new terminal window (in root of 'backend' folder) and run

`npm run seeds:all`

This will populate the database with some dummy data and 2 users
1. test@mail.com / 12345678
2. test2@mail.com / 12345678

- **Go into 'frontend' folder and run**

```
npm install
npm run start
```
